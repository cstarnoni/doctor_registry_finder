import csv
import getopt
import io
import os
import sys
import time
import urllib.parse
import shutil
from random import randint

from lxml import etree

import requests


def read_all_doctors():
    doctors_array_in = []

    medici_csv = os.path.join('..', 'medici', 'source', 'Medici.csv')
    with open(medici_csv, 'rt')as f:
        data = csv.reader(f)
        first = True
        for row in data:
            if not first:
                doctor_input = {
                    'name': row[1],
                    'surname': row[0]

                }
                doctors_array_in.append(doctor_input)
            first = False
        return doctors_array_in


def download_doctor_info(surname_raw, name_raw, iteration_index, total):

    name = urllib.parse.quote(name_raw)
    surname = urllib.parse.quote(surname_raw)
    post_param = 'p_l_id=16262&p_p_id=genericlist_WAR_laitumsportlet_INSTANCE_zCFf5bBop3s7&p_p_lifecycle=0&p_t_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=4&p_p_isolated=1&currentURL=%2Fricerca-medici&_genericlist_WAR_laitumsportlet_INSTANCE_zCFf5bBop3s7_resetCur=&_genericlist_WAR_laitumsportlet_INSTANCE_zCFf5bBop3s7_surname=' + surname + '&_genericlist_WAR_laitumsportlet_INSTANCE_zCFf5bBop3s7_taxCode=&portletAjaxable=1&_genericlist_WAR_laitumsportlet_INSTANCE_zCFf5bBop3s7_name=' + name
    # print(name)
    headers = {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    url = 'https://www.salutelazio.it/c/portal/render_portlet'
    response = requests.post(url, data=post_param, headers=headers)
    # print str(response)
    # print response.text

    parser = etree.HTMLParser()
    xhtml = etree.parse(io.StringIO(response.text), parser)
    root = xhtml.getroot()
    # print(root)
    subsections_with_info = root.findall(".//div[@class='span12']")
    doctor_info_div = None
    for subsection in subsections_with_info:
        contentmarker = subsection.findall(".//p[@class='text-uppercase']")
        if len(contentmarker) != 0:
            doctor_info_div = subsection
            break
    # clean
    if doctor_info_div is None:
        print ("workin " + str(iteration_index) + "/" + str(total) + " ...Miss :(  !")
        return False
    print ("workin " + str(iteration_index) + "/" + str(total) + "  ...HIT :)  !")
    images = doctor_info_div.findall(".//img")
    for image in images:
        image.getparent().remove(image)

    detail_file_content = "<html><body>" + etree.tostring(doctor_info_div, encoding="unicode") + "</body></html>"

    filename = os.path.join('..', 'output', 'detail', get_nome_file_dettaglio(name_raw, surname_raw))
    with open(filename, "w") as text_file_out:
        text_file_out.write(detail_file_content)
    return True


def get_nome_file_dettaglio(name, surname):
    return 'dettaglio_' + surname.replace(" ", "_") + "_" + name.replace(" ", "_") + ".html"


if __name__ == '__main__':
    start_idx = None
    end_idx = None
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hs:e:",["start=","end="])
    except getopt.GetoptError:
        print('csv_reader.py -s <start_doctor> -e <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('csv_reader.py -s <start_doctor> -e <end_doctor>')
            sys.exit()
        elif opt in ("-s", "--start"):
            start_idx = int(arg)
        elif opt in ("-e", "--end"):
            end_idx = int(arg)

    if start_idx is None:
        start_idx = 0

    doctors = read_all_doctors()
    if end_idx is None:
        end_idx = len(doctors) - 1

    if start_idx >= end_idx:
        print("Invalid range parameters")
        sys.exit(3)

    output_dir_path = os.path.join('..', 'output')
    if os.path.exists(output_dir_path):
        shutil.rmtree(output_dir_path)

    os.mkdir(output_dir_path)
    os.mkdir(os.path.join('..', 'output', 'detail'))
    print("Total Doctors in file " + str(len(doctors)))
    print("range selected from: " + str(start_idx) + " to " + str(end_idx))
    doctor_found = []
    doctor_not_found = []

    index_file_content = "<html><body>" \
                         "<h1> MEDICI </h1>" \
                         "<ul>"

    total_doc = end_idx - start_idx
    iteration = 1
    for doctor in doctors[start_idx:end_idx]:
        found = download_doctor_info(doctor['surname'], doctor['name'], iteration, total_doc)
        if found:
            doctor_found.append(doctor)
        else:
            doctor_not_found.append(doctor)
        time.sleep(randint(2, 5))
        iteration = iteration + 1

    sorted_found = sorted(doctor_found, key=lambda i: i['surname'])

    for found in sorted_found:
        index_file_content = index_file_content + "<li><a href=\"./detail/" + \
                             get_nome_file_dettaglio(found['name'], found['surname']) + \
                             "\" >" + found['surname'] + " " + found['name'] + " </a></li>"

    index_file_content = index_file_content + "</ul></body></html>"
    index_file_path = os.path.join('..', 'output', 'index.html')
    with open(index_file_path, "w") as text_file:
        text_file.write(index_file_content)
    print("FOUND " + str(len(doctor_found)) + "DOCTORS")
    print("")
    if len(doctor_not_found) != 0:
        print("THERE ARE " + str(len(doctor_not_found)) + " DOCTORS NOT FOUND PLEASE CHECK THE CSV ")
        for notfound in doctor_not_found:
            print(notfound)
